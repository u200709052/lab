public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area() {
        return this.sideA * this.sideB;
    }

    public int perimeter() {
        return 2 * (this.sideA + this.sideB);
    }

    public Point[] corners() {
        Point[] corners = new Point[]{this.topLeft, new Point(this.topLeft.xCoord + this.sideA, this.topLeft.yCoord), new Point(this.topLeft.xCoord + this.sideA, this.topLeft.yCoord - this.sideB), new Point(this.topLeft.xCoord, this.topLeft.yCoord - this.sideB)};
        return corners;
    }
}