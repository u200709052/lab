public class Point {
    int xCoord;
    int yCoord;

    public Point(int xy) {
        this.xCoord = xy;
        this.yCoord = xy;
    }
    public Point(int x, int y) {
        this.xCoord = x;
        this.yCoord = y;
    }

    public double distanceFromAnotherPoint(Point p) {
        int xDiff = this.xCoord - p.xCoord;
        int yDiff = this.yCoord - p.yCoord;
        return Math.sqrt(Math.pow((double)xDiff, 2.0) + Math.pow((double)yDiff, 2.0));
    }
}