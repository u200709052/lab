public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area() {
        return Math.PI * Math.pow((double)this.radius, 2.0);
    }

    public double perimeter() {
        return Math.PI * (double)this.radius;
    }

    public boolean intersect(Circle c) {
        return (double)(this.radius + c.radius) >= this.center.distanceFromAnotherPoint(c.center);
    }
}


