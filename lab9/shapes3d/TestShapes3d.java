package shapes3d;

public class TestShapes3d {
    public static void main(String[] args) {
        Cylinder cl = new Cylinder(5.0, 8.0);
        System.out.println(cl);
        System.out.println("Area of cylinder: " +cl.area());
        System.out.println("Volume of cylinder: " +cl.volume());

        Cube cb = new Cube(5.0);
        System.out.println(cb);
        System.out.println("Area of cube: " +cb.area());
        System.out.println("Volume of cube: " +cb.volume());

    }
}
