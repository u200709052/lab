public class GCDRec {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int smaller = a > b ? b : a;
        int greater = a < b ? b : a;

        int gcdResult = gcd(greater, smaller);

        System.out.println("GCD for " + greater + " and " + smaller + " is equal to " + gcdResult);

    }

    /**
     *
     * base condition: remainder ==0
     * gcd(greater,smaller) = gcd(smaller,greater%smaller)
     */
    private static int gcd(int smaller, int greater){
        if (smaller == 0)
            return greater;
        return gcd(smaller, greater % smaller);
    }
}
