package drawing.version3;

import shapes.Square;

import java.awt.*;
import java.util.ArrayList;


public class Drawing {

	private ArrayList<Shape> shapes = new ArrayList<Shape>();

	public double calculateTotalArea(){
		double totalArea = 0;
		//totalArea += s.area();
		for (Shape s : shapes) {
			System.out.println(s.getClass());
			System.out.println(s.area());
			totalArea += s.area();
		}

		return totalArea;
	}

	public void addShape (Square s) {
		shapes.add(s);
	}

}
