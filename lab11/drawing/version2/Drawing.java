package drawing.version2;

import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();

	public double calculateTotalArea(){
		double totalArea = 0;
		//totalArea += s.area();
		for (Object s : shapes){
			System.out.println(s.getClass());
			if (s instanceof Circle) {
				Circle c = (Circle) s;
				totalArea += c.area();
			} else if( s instanceof Rectangle){
				Rectangle r = (Rectangle) s;
				totalArea += r.area();
			}else if(s instanceof Square){
				Square sq = (Square) s;
				totalArea += sq.area();
			}

		}


		return totalArea;
	}
	
	public void addShape (Object s) {
		shapes.add(s);
	}
	
}
