public class MyDate {
    private int day, month, year;
    private int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public void incrementDay() {
        int newDay = day + 1;
        if (newDay > maxDays[month]) {
            day = 1;
            incrementMonth();
        } else if (month == 1 && newDay == 29 && !isLeapYear()) {
            day = 1;
            incrementMonth();
        } else {
            day = newDay;
        }
    }

    private boolean isLeapYear() {
        boolean leap = false;
        if (this.year % 4 == 0) {
            if (this.year % 100 == 0) {
                if (this.year % 400 == 0) {
                    leap = true;
                } else {
                    leap = false;
                }
            } else {
                leap = true;
            }
        } else {
            leap = false;
        }

        return leap;
    }

    public void decrementDay() {
        int newDay = day - 1;
        if (newDay == 0) {
            decrementMonth();
            day = maxDays[month];
        } else {
            day = newDay;
        }

    }

    public void incrementDay(int i) {
        while(i > 0) {
            incrementDay();
            --i;
        }

    }

    public void decrementDay(int i) {
        while(i > 0) {
            decrementDay();
            --i;
        }

    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 1 && day == 29 && !isLeapYear()) {
            day = 28;
        }

    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementYear(int i) {
        incrementYear(-i);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void decrementMonth() {
        incrementMonth(-1);
    }

    public void decrementMonth(int i) {
        incrementMonth(-i);
    }

    public void incrementMonth(int diff) {
        int newMonth = (month + diff) % 12;
        int yearChange = 0;
        if (newMonth < 0) {
            newMonth += 12;
            yearChange = -1;
        }

        yearChange += (month + diff) / 12;
        month = newMonth;
        year += yearChange;
        if (day > maxDays[month]) {
            day = maxDays[month];
            if (month == 1 && day == 29 && !isLeapYear()) {
                day = 28;
            }
        }

    }
    public boolean isBefore(MyDate anotherDate){
        return false;
    }

    public boolean isAfter(MyDate anotherDate){
        return true;
    }

    public int dayDifference(MyDate anotherDate){
        int diff = 0;
        if (isBefore((anotherDate))){

        }else if (isAfter(anotherDate)){

        }
        return diff;
    }

    public void incrementMonth() {
        incrementMonth(1);
    }
    public String toString() {
        return year + "-" + (month + 1 < 10 ? "0" : "") + (month + 1) + "-" + (day < 10 ? "0" : "") + day;
    }
}