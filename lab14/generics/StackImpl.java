package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StackImpl<T> implements Stack<T>{
    StackItem <T> top = null;

    @Override
    public void push(T item) {
        StackItem<T> elem = new StackItem<T>(item);
        StackItem<T> prevTop = top;
        top = elem;
        top.setNext(prevTop);

    }

    @Override
    public T pop() {
        StackItem<T> prevTop = top;
        top = prevTop.getNext();
        return prevTop.getItem();
    }

    @Override
    public boolean empty() {
        return top ==null;
    }

    @Override
    public List<T> toList() {
        ArrayList<T> list = new ArrayList<>();
        StackItem<T> item = top;
        while (item != null){
            list.add(item.getItem());
            item = item.getNext();
        }
        return list;
    }

}
