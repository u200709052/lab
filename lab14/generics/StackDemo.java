package generics;

import java.awt.*;

public class StackDemo {
    public static void main(String[] args) {

        Stack<Shape> shapeStack = new StackImpl<>();
        shapeStack.push(new Circle(4));
        shapeStack.push(new Rectangle(5,6));
        shapeStack.push(new Square(7));

        System.out.println(shapeStack.toList());

        Stack<Circle> circleStack = new StackImpl<>();
        circleStack.push(new Circle(10));
        circleStack.push(new Circle(11));

        System.out.println(circleStack.toList());

        shapeStack.addAll(circleStack);
        System.out.println(shapeStack);


        Stack<Integer> stack = new StackImpl<>();
        stack.push(1);
        stack.push(2);
        stack.push(5);
        stack.push(3);
        stack.push(6);
        stack.push(7);

        System.out.println(stack.toList());

        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(1);
        stack2.push(2);
        stack2.push(5);

        System.out.println(stack2.toList());

        stack.addAll(stack2);
        System.out.println(stack.toList());

        while (!stack2.empty()){
            System.out.println(stack2.pop());
        }


    }
}
