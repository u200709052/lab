package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StackArrayImpl implements  Stack{
    ArrayList<Object> stack = new ArrayList<Object>();

    @Override
    public void push(Object item) {
        stack.add(0,item);
    }

    @Override
    public Object pop() {
        return stack.remove(0);
    }

    @Override
    public boolean empty() {
        return stack.size() ==0;
    }

    @Override
    public List toList() {
        return stack;
    }


}
